# Argo

| Service Name        | Location/Site | Roles & Responsibility         | Namespace |
|---------------------|---------------|--------------------------------|-----------|
| argo-ui             | GC            | Continuous Delivery Console UI | argocd    |
| argocd-redis        | GC            | Cache for ArgoCD               | argocd    |
| argocd-server       | GC            | Continuous Delivery Server     | argocd    |
| slack-gateway-svc   | GC            | Slack GW                       | argocd    |
| slack-sensor        | GC            | Slack notifications	           | argocd    |
| webhook-gateway-svc | GC            | Webhook for CI notifications   | argocd    |
