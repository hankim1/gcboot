# Stage 0

- [ ] [Clone the deployment repo](./001-clone-repos/README.md)
- [ ] [Install & Setup Tools](./002-setup-tools/)
- [ ] [Prepare credentials to access AWS APIs](./003-prep-aws-credentials/)
- [ ] [Create IAM users/groups/policies/roles](./004-create-iam/)

## Reference
- [GC Deployment w/ Step 0](https://gitlab.com/volterra/ves.io/sre-deployment/-/wikis/deployment/GC-Deployment#stage-0-procedures)
