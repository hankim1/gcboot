# Results & Guide After Installing TF, EKSCTL, AWSCLI

This is to capture guideline and environment variables just in case after installing terraform, eksctl and awscli.

```bash
$ brew install terraform eksctl awscli
eksctl 0.118.0 is already installed but outdated (so it will be upgraded).
==> Fetching terraform
==> Downloading https://ghcr.io/v2/homebrew/core/terraform/manifests/1.5.0
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/terraform/blobs/sha256:c29d1d26b17e7e51d89d0d7b3cfd21c35dd4636aba9527b8459e02566a38347c
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:c29d1d26b17e7e51d89d0d7b3cfd21c35dd4636aba9527b8459e02566a38347c?se=2023-06-17T10%3A20%3A00Z&sig=dPgcvM%2F6f5RyBdK
################################################################################################################################################################################################ 100.0%
==> Fetching dependencies for eksctl: aws-iam-authenticator
==> Fetching aws-iam-authenticator
==> Downloading https://ghcr.io/v2/homebrew/core/aws-iam-authenticator/manifests/0.6.2
Already downloaded: /Users/hankim/Library/Caches/Homebrew/downloads/e251185a1ea710821d02632d0f3ad0125077b35afb2d18c3277d77da16be0f70--aws-iam-authenticator-0.6.2.bottle_manifest.json
==> Downloading https://ghcr.io/v2/homebrew/core/aws-iam-authenticator/blobs/sha256:803311bd323f8f3cb1a9dc87244379c1015f86384e9d029d0f5eaad7421fee2c

Already downloaded: /Users/hankim/Library/Caches/Homebrew/downloads/9643c901cdaac7a359d8288ef9aeb2234dec80ebd43e817b361990990109bfaf--aws-iam-authenticator--0.6.2.ventura.bottle.tar.gz
==> Fetching eksctl
==> Downloading https://ghcr.io/v2/homebrew/core/eksctl/manifests/0.145.0
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/eksctl/blobs/sha256:f78e0902a41bce149387cb3be407f2afe6282bcfe3ef0f9209f73620466da8d8
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:f78e0902a41bce149387cb3be407f2afe6282bcfe3ef0f9209f73620466da8d8?se=2023-06-17T10%3A20%3A00Z&sig=dthJfAfrjaUFjBgV5
################################################################################################################################################################################################ 100.0%
==> Fetching dependencies for awscli: ca-certificates, openssl@1.1, sqlite, python@3.11 and docutils
==> Fetching ca-certificates
==> Downloading https://ghcr.io/v2/homebrew/core/ca-certificates/manifests/2023-05-30
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/ca-certificates/blobs/sha256:f664c0f185677a82689ada2a4e35c555e48885e6c2fb5e2dfcc82d9fb79cf870
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:f664c0f185677a82689ada2a4e35c555e48885e6c2fb5e2dfcc82d9fb79cf870?se=2023-06-17T10%3A20%3A00Z&sig=RsimGCaWNHIHUkbUH
################################################################################################################################################################################################ 100.0%
==> Fetching openssl@1.1
==> Downloading https://ghcr.io/v2/homebrew/core/openssl/1.1/manifests/1.1.1u
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/openssl/1.1/blobs/sha256:f0a7ffd1772a1729d5f48d2e56b3a71af27d3f985598d3e2509359a8f60edee5
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:f0a7ffd1772a1729d5f48d2e56b3a71af27d3f985598d3e2509359a8f60edee5?se=2023-06-17T10%3A20%3A00Z&sig=9nZD9uZBB6i%2BmGz
################################################################################################################################################################################################ 100.0%
==> Fetching sqlite
==> Downloading https://ghcr.io/v2/homebrew/core/sqlite/manifests/3.42.0
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/sqlite/blobs/sha256:4bbf2bd9382c9f257712e60ff36fcddc7de69fcb95a06006558673f6985da85d
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:4bbf2bd9382c9f257712e60ff36fcddc7de69fcb95a06006558673f6985da85d?se=2023-06-17T10%3A20%3A00Z&sig=BA0knw%2FHc8h2F4w
################################################################################################################################################################################################ 100.0%
==> Fetching python@3.11
==> Downloading https://ghcr.io/v2/homebrew/core/python/3.11/manifests/3.11.4
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/python/3.11/blobs/sha256:b0df9e62dded2ce44e9362a2fa7b678f03748c47a4c76709c5213f4c2c1d4c38
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:b0df9e62dded2ce44e9362a2fa7b678f03748c47a4c76709c5213f4c2c1d4c38?se=2023-06-17T10%3A20%3A00Z&sig=A46IJG%2FXys5aAdD
################################################################################################################################################################################################ 100.0%
==> Fetching docutils
==> Downloading https://ghcr.io/v2/homebrew/core/docutils/manifests/0.20.1
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/docutils/blobs/sha256:ea0e9988733ecd494fb9b5ec6479f25eeb11718f1614fc92f0174f44acc913bb
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:ea0e9988733ecd494fb9b5ec6479f25eeb11718f1614fc92f0174f44acc913bb?se=2023-06-17T10%3A20%3A00Z&sig=%2B2gliLI5Z2HtVok
################################################################################################################################################################################################ 100.0%
==> Fetching awscli
==> Downloading https://ghcr.io/v2/homebrew/core/awscli/manifests/2.12.1
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/awscli/blobs/sha256:f5b0f5a39b45081bc6f676a47d284fc971ffd21badad3f931dba4bd3419f931c
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:f5b0f5a39b45081bc6f676a47d284fc971ffd21badad3f931dba4bd3419f931c?se=2023-06-17T10%3A20%3A00Z&sig=AGbY1rAfGkg7AKbP7
################################################################################################################################################################################################ 100.0%
==> Pouring terraform--1.5.0.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/terraform/1.5.0: 6 files, 69.1MB
==> Running `brew cleanup terraform`...
Disable this behaviour by setting HOMEBREW_NO_INSTALL_CLEANUP.
Hide these hints with HOMEBREW_NO_ENV_HINTS (see `man brew`).
==> Upgrading eksctl
  0.118.0 -> 0.145.0 

==> Installing dependencies for eksctl: aws-iam-authenticator
==> Installing eksctl dependency: aws-iam-authenticator
==> Pouring aws-iam-authenticator--0.6.2.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/aws-iam-authenticator/0.6.2: 6 files, 51.0MB
==> Installing eksctl
==> Pouring eksctl--0.145.0.ventura.bottle.tar.gz
==> Caveats
Bash completion has been installed to:
  /usr/local/etc/bash_completion.d
==> Summary
🍺  /usr/local/Cellar/eksctl/0.145.0: 8 files, 139.8MB
==> Running `brew cleanup eksctl`...
Removing: /usr/local/Cellar/eksctl/0.118.0... (5 files, 130.4MB)
==> Installing dependencies for awscli: ca-certificates, openssl@1.1, sqlite, python@3.11 and docutils
==> Installing awscli dependency: ca-certificates
==> Pouring ca-certificates--2023-05-30.ventura.bottle.tar.gz
==> Regenerating CA certificate bundle from keychain, this may take a while...
🍺  /usr/local/Cellar/ca-certificates/2023-05-30: 3 files, 216.2KB
==> Installing awscli dependency: openssl@1.1
==> Pouring openssl@1.1--1.1.1u.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/openssl@1.1/1.1.1u: 8,101 files, 18.5MB
==> Installing awscli dependency: sqlite
==> Pouring sqlite--3.42.0.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/sqlite/3.42.0: 11 files, 4.5MB
==> Installing awscli dependency: python@3.11
==> Pouring python@3.11--3.11.4.ventura.bottle.tar.gz
Error: The `brew link` step did not complete successfully
The formula built, but is not symlinked into /usr/local
Could not symlink bin/2to3
Target /usr/local/bin/2to3
already exists. You may want to remove it:
  rm '/usr/local/bin/2to3'

To force the link and overwrite all conflicting files:
  brew link --overwrite python@3.11

To list all files that would be deleted:
  brew link --overwrite --dry-run python@3.11

Possible conflicting files are:
/usr/local/bin/2to3 -> /Library/Frameworks/Python.framework/Versions/3.7/bin/2to3
/usr/local/bin/idle3 -> /Library/Frameworks/Python.framework/Versions/3.7/bin/idle3
/usr/local/bin/pydoc3 -> /Library/Frameworks/Python.framework/Versions/3.7/bin/pydoc3
/usr/local/bin/python3 -> /Library/Frameworks/Python.framework/Versions/3.7/bin/python3
/usr/local/bin/python3-config -> /Library/Frameworks/Python.framework/Versions/3.7/bin/python3-config
==> /usr/local/Cellar/python@3.11/3.11.4/bin/python3.11 -m ensurepip
==> /usr/local/Cellar/python@3.11/3.11.4/bin/python3.11 -m pip install -v --no-deps --no-index --upgrade --isolated --target=/usr/local/lib/python3.11/site-packages /usr/local/Cellar/python@3.11/3.11
==> Summary
🍺  /usr/local/Cellar/python@3.11/3.11.4: 3,286 files, 61.2MB
==> Installing awscli dependency: docutils
==> Pouring docutils--0.20.1.all.bottle.tar.gz
Error: The `brew link` step did not complete successfully
The formula built, but is not symlinked into /usr/local
Could not symlink bin/rst2html.py
Target /usr/local/bin/rst2html.py
already exists. You may want to remove it:
  rm '/usr/local/bin/rst2html.py'

To force the link and overwrite all conflicting files:
  brew link --overwrite docutils

To list all files that would be deleted:
  brew link --overwrite --dry-run docutils

Possible conflicting files are:
/usr/local/bin/rst2html.py
/usr/local/bin/rst2html4.py
/usr/local/bin/rst2html5.py
/usr/local/bin/rst2latex.py
/usr/local/bin/rst2man.py
/usr/local/bin/rst2odt.py
/usr/local/bin/rst2odt_prepstyles.py
/usr/local/bin/rst2pseudoxml.py
/usr/local/bin/rst2s5.py
/usr/local/bin/rst2xetex.py
/usr/local/bin/rst2xml.py
/usr/local/bin/rstpep2html.py
==> Summary
🍺  /usr/local/Cellar/docutils/0.20.1: 231 files, 2MB
==> Installing awscli
==> Pouring awscli--2.12.1.ventura.bottle.tar.gz
Error: The `brew link` step did not complete successfully
The formula built, but is not symlinked into /usr/local
Could not symlink bin/aws
Target /usr/local/bin/aws
already exists. You may want to remove it:
  rm '/usr/local/bin/aws'

To force the link and overwrite all conflicting files:
  brew link --overwrite awscli

To list all files that would be deleted:
  brew link --overwrite --dry-run awscli

Possible conflicting files are:
/usr/local/bin/aws -> /usr/local/aws-cli/aws
/usr/local/bin/aws_completer -> /usr/local/aws-cli/aws_completer
==> Caveats
The "examples" directory has been installed to:
  /usr/local/share/awscli/examples

Bash completion has been installed to:
  /usr/local/etc/bash_completion.d
==> Summary
🍺  /usr/local/Cellar/awscli/2.12.1: 13,304 files, 119.7MB
==> Running `brew cleanup awscli`...
Removing: /usr/local/Cellar/awscli/2.11.20... (13,277 files, 118.0MB)
==> Upgrading 13 dependents of upgraded formulae:
Disable this behaviour by setting HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK.
Hide these hints with HOMEBREW_NO_ENV_HINTS (see `man brew`).
azure-cli 2.48.1 -> 2.49.0, curl 8.0.1 -> 8.1.2, krb5 1.20.1 -> 1.21, libpq 15.2 -> 15.3, llvm 16.0.3 -> 16.0.5, nghttp2 1.51.0 -> 1.54.0_1, openssl@3 3.1.0 -> 3.1.1, perl 5.36.0 -> 5.36.1, python@3.10 3.10.11 -> 3.10.12, python@3.8 3.8.16 -> 3.8.17, python@3.9 3.9.16 -> 3.9.17, tcl-tk 8.6.13 -> 8.6.13_3, wget 1.21.3_1 -> 1.21.4
==> Fetching krb5
==> Downloading https://ghcr.io/v2/homebrew/core/krb5/manifests/1.21
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/krb5/blobs/sha256:dfc056001cd7d62d3278e27a6912a6925a9b3ff835e4e925a2ec76e5519e6f7f
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:dfc056001cd7d62d3278e27a6912a6925a9b3ff835e4e925a2ec76e5519e6f7f?se=2023-06-17T10%3A20%3A00Z&sig=okyttiXaWphQih4hG
################################################################################################################################################################################################ 100.0%
==> Fetching libpq
==> Downloading https://ghcr.io/v2/homebrew/core/libpq/manifests/15.3
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/libpq/blobs/sha256:a0731fabb26973dc162f66f4a5741c1548416f7f9bf9e7d8a8eb7911d19af65a
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:a0731fabb26973dc162f66f4a5741c1548416f7f9bf9e7d8a8eb7911d19af65a?se=2023-06-17T10%3A20%3A00Z&sig=8GyTtpmNAkXMatGKY
################################################################################################################################################################################################ 100.0%
==> Fetching python@3.10
==> Downloading https://ghcr.io/v2/homebrew/core/python/3.10/manifests/3.10.12
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/python/3.10/blobs/sha256:9ff1027548d103e245591d445f7479e69f9d232ecd06caf3f64fe16c440b3524
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:9ff1027548d103e245591d445f7479e69f9d232ecd06caf3f64fe16c440b3524?se=2023-06-17T10%3A20%3A00Z&sig=KpuQlimZeKF1pFUw4
################################################################################################################################################################################################ 100.0%
==> Fetching python@3.9
==> Downloading https://ghcr.io/v2/homebrew/core/python/3.9/manifests/3.9.17
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/python/3.9/blobs/sha256:03ba0e6a52ab73af4cc94b66a9f7613e55a01dcfc5b4683b63b6ae0fc07f628b
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:03ba0e6a52ab73af4cc94b66a9f7613e55a01dcfc5b4683b63b6ae0fc07f628b?se=2023-06-17T10%3A20%3A00Z&sig=DEWzeJ2RFxzLmU%2F
################################################################################################################################################################################################ 100.0%
==> Fetching llvm
==> Downloading https://ghcr.io/v2/homebrew/core/llvm/manifests/16.0.5
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/llvm/blobs/sha256:aabb516ca05aa2e37cfe2417bfee9de6b95a15ef99ee91999cedcbe793bcd5fc
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:aabb516ca05aa2e37cfe2417bfee9de6b95a15ef99ee91999cedcbe793bcd5fc?se=2023-06-17T10%3A20%3A00Z&sig=2zywiXbgG%2FrLhbU
################################################################################################################################################################################################ 100.0%
==> Fetching dependencies for curl: libnghttp2 and libssh2
==> Fetching libnghttp2
==> Downloading https://ghcr.io/v2/homebrew/core/libnghttp2/manifests/1.54.0
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/libnghttp2/blobs/sha256:6af7160a98f7d5f4c18327a71e54dc55c6d136d16634204ddd235d16e9cae4bd
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:6af7160a98f7d5f4c18327a71e54dc55c6d136d16634204ddd235d16e9cae4bd?se=2023-06-17T10%3A25%3A00Z&sig=TWWY2qprlDGe0YbDh
################################################################################################################################################################################################ 100.0%
==> Fetching libssh2
==> Downloading https://ghcr.io/v2/homebrew/core/libssh2/manifests/1.11.0
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/libssh2/blobs/sha256:037e14a2a1c76d5019ab96264574e72e652864da0e01f373e4183c893108f064
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:037e14a2a1c76d5019ab96264574e72e652864da0e01f373e4183c893108f064?se=2023-06-17T10%3A25%3A00Z&sig=GDxujO84lg%2BvFhM
################################################################################################################################################################################################ 100.0%
==> Fetching curl
==> Downloading https://ghcr.io/v2/homebrew/core/curl/manifests/8.1.2
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/curl/blobs/sha256:fc1fddfaadaa7ee02e512a066be385cf4ae9a2b97d6bdfcf7f022dd58354c76b
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:fc1fddfaadaa7ee02e512a066be385cf4ae9a2b97d6bdfcf7f022dd58354c76b?se=2023-06-17T10%3A25%3A00Z&sig=l8fUbvawUL%2BcL4L
################################################################################################################################################################################################ 100.0%
==> Fetching openssl@3
==> Downloading https://ghcr.io/v2/homebrew/core/openssl/3/manifests/3.1.1
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/openssl/3/blobs/sha256:a2a6fca7d2c56c542508c4d274b7a466ac183d7304e8268c94a2eee7949b8c5e
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:a2a6fca7d2c56c542508c4d274b7a466ac183d7304e8268c94a2eee7949b8c5e?se=2023-06-17T10%3A25%3A00Z&sig=b7j7pUveGTIQeivKe
################################################################################################################################################################################################ 100.0%
==> Fetching tcl-tk
==> Downloading https://ghcr.io/v2/homebrew/core/tcl-tk/manifests/8.6.13_3-1
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/tcl-tk/blobs/sha256:15cade8287fb43f440cbd2de43b49942034bb75104180add1768baa155e3f6b1
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:15cade8287fb43f440cbd2de43b49942034bb75104180add1768baa155e3f6b1?se=2023-06-17T10%3A25%3A00Z&sig=AuzvzhJ5YxHKdHk81
################################################################################################################################################################################################ 100.0%
==> Fetching azure-cli
==> Downloading https://ghcr.io/v2/homebrew/core/azure-cli/manifests/2.49.0
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/azure-cli/blobs/sha256:ed481f1a1bc4365af6bf96e9aaccd88ffc8adbf24e0f75bcc2ca8c9c464b9673
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:ed481f1a1bc4365af6bf96e9aaccd88ffc8adbf24e0f75bcc2ca8c9c464b9673?se=2023-06-17T10%3A25%3A00Z&sig=kAYbQGNkgtGTWb9gx
################################################################################################################################################################################################ 100.0%
==> Fetching wget
==> Downloading https://ghcr.io/v2/homebrew/core/wget/manifests/1.21.4
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/wget/blobs/sha256:f1d0f59e9cd5863d4d4e29a4f0d7cf1c34da8ab4535d9b9a7b8822dbc4ce5e1b
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:f1d0f59e9cd5863d4d4e29a4f0d7cf1c34da8ab4535d9b9a7b8822dbc4ce5e1b?se=2023-06-17T10%3A25%3A00Z&sig=RbfltypmeHj8andBy
################################################################################################################################################################################################ 100.0%
==> Fetching dependencies for nghttp2: c-ares
==> Fetching c-ares
==> Downloading https://ghcr.io/v2/homebrew/core/c-ares/manifests/1.19.1
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/c-ares/blobs/sha256:504f6347444d599983e075211dee95529f7329324f3f7470914adebf06f46419
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:504f6347444d599983e075211dee95529f7329324f3f7470914adebf06f46419?se=2023-06-17T10%3A25%3A00Z&sig=9B2Jp7kU9UJUVkyzg
################################################################################################################################################################################################ 100.0%
==> Fetching nghttp2
==> Downloading https://ghcr.io/v2/homebrew/core/nghttp2/manifests/1.54.0_1
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/nghttp2/blobs/sha256:dfffb690be35c18a17d33ae3a22fdf6cdecf07210384a1bd6cd6ae6fb604f3aa
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:dfffb690be35c18a17d33ae3a22fdf6cdecf07210384a1bd6cd6ae6fb604f3aa?se=2023-06-17T10%3A25%3A00Z&sig=Rd4x39ggfPgt0j%2F
################################################################################################################################################################################################ 100.0%
==> Fetching perl
==> Downloading https://ghcr.io/v2/homebrew/core/perl/manifests/5.36.1
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/perl/blobs/sha256:12a8c480c692775bf24c4ba802103982299e899c9d949ec4bc288ecab661a42d
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:12a8c480c692775bf24c4ba802103982299e899c9d949ec4bc288ecab661a42d?se=2023-06-17T10%3A25%3A00Z&sig=a4zYcptLEvdBZ%2FW
################################################################################################################################################################################################ 100.0%
==> Fetching python@3.8
==> Downloading https://ghcr.io/v2/homebrew/core/python/3.8/manifests/3.8.17
################################################################################################################################################################################################ 100.0%
==> Downloading https://ghcr.io/v2/homebrew/core/python/3.8/blobs/sha256:78273c768df95a36c47cec3f757e33af8613cdeb06c6d29abeff4b429feb2e09
==> Downloading from https://pkg-containers.githubusercontent.com/ghcr1/blobs/sha256:78273c768df95a36c47cec3f757e33af8613cdeb06c6d29abeff4b429feb2e09?se=2023-06-17T10%3A25%3A00Z&sig=xkIgWQy2E6FjW3KfV
################################################################################################################################################################################################ 100.0%
==> Upgrading krb5
  1.20.1 -> 1.21 

==> Pouring krb5--1.21.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/krb5/1.21: 162 files, 4.9MB
==> Running `brew cleanup krb5`...
Removing: /usr/local/Cellar/krb5/1.20.1... (162 files, 5.1MB)
==> Upgrading libpq
  15.2 -> 15.3 

==> Pouring libpq--15.3.ventura.bottle.tar.gz
==> Caveats
libpq is keg-only, which means it was not symlinked into /usr/local,
because conflicts with postgres formula.

If you need to have libpq first in your PATH, run:
  echo 'export PATH="/usr/local/opt/libpq/bin:$PATH"' >> /Users/hankim/.bash_profile

For compilers to find libpq you may need to set:
  export LDFLAGS="-L/usr/local/opt/libpq/lib"
  export CPPFLAGS="-I/usr/local/opt/libpq/include"

For pkg-config to find libpq you may need to set:
  export PKG_CONFIG_PATH="/usr/local/opt/libpq/lib/pkgconfig"
==> Summary
🍺  /usr/local/Cellar/libpq/15.3: 2,369 files, 28MB
==> Running `brew cleanup libpq`...
Removing: /usr/local/Cellar/libpq/15.2... (2,368 files, 28MB)
==> Upgrading python@3.10
  3.10.11 -> 3.10.12 

==> Pouring python@3.10--3.10.12.ventura.bottle.tar.gz
==> /usr/local/Cellar/python@3.10/3.10.12/bin/python3.10 -m ensurepip
==> /usr/local/Cellar/python@3.10/3.10.12/bin/python3.10 -m pip install -v --no-deps --no-index --upgrade --isolated --target=/usr/local/lib/python3.10/site-packages /usr/local/Cellar/python@3.10/3.1
==> Caveats
Python has been installed as
  /usr/local/bin/python3.10

Unversioned and major-versioned symlinks `python`, `python3`, `python-config`, `python3-config`, `pip`, `pip3`, etc. pointing to
`python3.10`, `python3.10-config`, `pip3.10` etc., respectively, have been installed into
  /usr/local/opt/python@3.10/libexec/bin

You can install Python packages with
  pip3.10 install <package>
They will install into the site-package directory
  /usr/local/lib/python3.10/site-packages

tkinter is no longer included with this formula, but it is available separately:
  brew install python-tk@3.10

If you do not need a specific version of Python, and always want Homebrew's `python3` in your PATH:
  brew install python3

See: https://docs.brew.sh/Homebrew-and-Python
==> Summary
🍺  /usr/local/Cellar/python@3.10/3.10.12: 3,097 files, 56.1MB
==> Running `brew cleanup python@3.10`...
Removing: /usr/local/Cellar/python@3.10/3.10.11... (3,133 files, 57MB)
==> Upgrading python@3.9
  3.9.16 -> 3.9.17 

==> Pouring python@3.9--3.9.17.ventura.bottle.tar.gz
==> /usr/local/Cellar/python@3.9/3.9.17/bin/python3.9 -m ensurepip
==> /usr/local/Cellar/python@3.9/3.9.17/bin/python3.9 -m pip install -v --no-deps --no-index --upgrade --isolated --target=/usr/local/lib/python3.9/site-packages /usr/local/Cellar/python@3.9/3.9.17/F
==> Caveats
Python has been installed as
  /usr/local/bin/python3.9

Unversioned and major-versioned symlinks `python`, `python3`, `python-config`, `python3-config`, `pip`, `pip3`, etc. pointing to
`python3.9`, `python3.9-config`, `pip3.9` etc., respectively, have been installed into
  /usr/local/opt/python@3.9/libexec/bin

You can install Python packages with
  pip3.9 install <package>
They will install into the site-package directory
  /usr/local/lib/python3.9/site-packages

tkinter is no longer included with this formula, but it is available separately:
  brew install python-tk@3.9

If you do not need a specific version of Python, and always want Homebrew's `python3` in your PATH:
  brew install python3

See: https://docs.brew.sh/Homebrew-and-Python
==> Summary
🍺  /usr/local/Cellar/python@3.9/3.9.17: 3,066 files, 55.2MB
==> Running `brew cleanup python@3.9`...
Removing: /usr/local/Cellar/python@3.9/3.9.16... (3,066 files, 55.4MB)
==> Upgrading llvm
  16.0.3 -> 16.0.5 

==> Pouring llvm--16.0.5.ventura.bottle.tar.gz
==> Caveats
To use the bundled libc++ please add the following LDFLAGS:
  LDFLAGS="-L/usr/local/opt/llvm/lib/c++ -Wl,-rpath,/usr/local/opt/llvm/lib/c++"

llvm is keg-only, which means it was not symlinked into /usr/local,
because macOS already provides this software and installing another version in
parallel can cause all kinds of trouble.

If you need to have llvm first in your PATH, run:
  echo 'export PATH="/usr/local/opt/llvm/bin:$PATH"' >> /Users/hankim/.bash_profile

For compilers to find llvm you may need to set:
  export LDFLAGS="-L/usr/local/opt/llvm/lib"
  export CPPFLAGS="-I/usr/local/opt/llvm/include"
==> Summary
🍺  /usr/local/Cellar/llvm/16.0.5: 6,779 files, 1.6GB
==> Running `brew cleanup llvm`...
Removing: /usr/local/Cellar/llvm/16.0.3... (6,779 files, 1.6GB)
==> Upgrading curl
  8.0.1 -> 8.1.2 

==> Installing dependencies for curl: libnghttp2 and libssh2
==> Installing curl dependency: libnghttp2
==> Pouring libnghttp2--1.54.0.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/libnghttp2/1.54.0: 13 files, 714.5KB
==> Installing curl dependency: libssh2
==> Pouring libssh2--1.11.0.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/libssh2/1.11.0: 197 files, 1.1MB
==> Installing curl
==> Pouring curl--8.1.2.ventura.bottle.tar.gz
==> Caveats
curl is keg-only, which means it was not symlinked into /usr/local,
because macOS already provides this software and installing another version in
parallel can cause all kinds of trouble.

If you need to have curl first in your PATH, run:
  echo 'export PATH="/usr/local/opt/curl/bin:$PATH"' >> /Users/hankim/.bash_profile

For compilers to find curl you may need to set:
  export LDFLAGS="-L/usr/local/opt/curl/lib"
  export CPPFLAGS="-I/usr/local/opt/curl/include"

For pkg-config to find curl you may need to set:
  export PKG_CONFIG_PATH="/usr/local/opt/curl/lib/pkgconfig"
==> Summary
🍺  /usr/local/Cellar/curl/8.1.2: 510 files, 4.2MB
==> Running `brew cleanup curl`...
Removing: /usr/local/Cellar/curl/8.0.1... (510 files, 4.2MB)
==> Upgrading openssl@3
  3.1.0 -> 3.1.1 

==> Pouring openssl@3--3.1.1.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/openssl@3/3.1.1: 6,495 files, 29.9MB
==> Running `brew cleanup openssl@3`...
Removing: /usr/local/Cellar/openssl@3/3.1.0... (6,494 files, 29.9MB)
==> Upgrading tcl-tk
  8.6.13 -> 8.6.13_3 

==> Pouring tcl-tk--8.6.13_3.ventura.bottle.1.tar.gz
🍺  /usr/local/Cellar/tcl-tk/8.6.13_3: 3,064 files, 52.8MB
==> Running `brew cleanup tcl-tk`...
Removing: /usr/local/Cellar/tcl-tk/8.6.13... (3,070 files, 52.8MB)
==> Upgrading azure-cli
  2.48.1 -> 2.49.0 

==> Pouring azure-cli--2.49.0.ventura.bottle.tar.gz
==> Caveats
Bash completion has been installed to:
  /usr/local/etc/bash_completion.d
==> Summary
🍺  /usr/local/Cellar/azure-cli/2.49.0: 22,186 files, 480.6MB
==> Running `brew cleanup azure-cli`...
Removing: /usr/local/Cellar/azure-cli/2.48.1... (23,886 files, 498.5MB)
==> Upgrading wget
  1.21.3_1 -> 1.21.4 

==> Pouring wget--1.21.4.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/wget/1.21.4: 91 files, 4.4MB
==> Running `brew cleanup wget`...
Removing: /usr/local/Cellar/wget/1.21.3_1... (89 files, 4.2MB)
==> Upgrading nghttp2
  1.51.0 -> 1.54.0_1 

==> Installing dependencies for nghttp2: c-ares
==> Installing nghttp2 dependency: c-ares
==> Pouring c-ares--1.19.1.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/c-ares/1.19.1: 87 files, 661KB
==> Installing nghttp2
==> Pouring nghttp2--1.54.0_1.ventura.bottle.tar.gz
🍺  /usr/local/Cellar/nghttp2/1.54.0_1: 17 files, 2.2MB
==> Running `brew cleanup nghttp2`...
Removing: /usr/local/Cellar/nghttp2/1.51.0... (17 files, 2.3MB)
==> Upgrading perl
  5.36.0 -> 5.36.1 

==> Pouring perl--5.36.1.ventura.bottle.tar.gz
==> Caveats
By default non-brewed cpan modules are installed to the Cellar. If you wish
for your modules to persist across updates we recommend using `local::lib`.

You can set that up like this:
  PERL_MM_OPT="INSTALL_BASE=$HOME/perl5" cpan local::lib
And add the following to your shell profile e.g. ~/.profile or ~/.zshrc
  eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib=$HOME/perl5)"
==> Summary
🍺  /usr/local/Cellar/perl/5.36.1: 2,492 files, 67.6MB
==> Running `brew cleanup perl`...
Removing: /usr/local/Cellar/perl/5.36.0... (2,490 files, 66.8MB)
==> Upgrading python@3.8
  3.8.16 -> 3.8.17 

==> Pouring python@3.8--3.8.17.ventura.bottle.tar.gz
==> /usr/local/Cellar/python@3.8/3.8.17/bin/python3.8 -s setup.py --no-user-cfg install --force --verbose --install-scripts=/usr/local/Cellar/python@3.8/3.8.17/bin --install-lib=/usr/local/lib/python
==> /usr/local/Cellar/python@3.8/3.8.17/bin/python3.8 -s setup.py --no-user-cfg install --force --verbose --install-scripts=/usr/local/Cellar/python@3.8/3.8.17/bin --install-lib=/usr/local/lib/python
==> /usr/local/Cellar/python@3.8/3.8.17/bin/python3.8 -s setup.py --no-user-cfg install --force --verbose --install-scripts=/usr/local/Cellar/python@3.8/3.8.17/bin --install-lib=/usr/local/lib/python
==> Downloading https://formulae.brew.sh/api/formula.jws.json
##O=-#      #                                                                                                                                                                                         
🍺  /usr/local/Cellar/python@3.8/3.8.17: 5,021 files, 80.6MB
==> Running `brew cleanup python@3.8`...
Removing: /usr/local/Cellar/python@3.8/3.8.16... (4,969 files, 79.1MB)
==> Checking for dependents of upgraded formulae...
==> No broken dependents found!
==> Caveats
==> eksctl
Bash completion has been installed to:
  /usr/local/etc/bash_completion.d
==> awscli
The "examples" directory has been installed to:
  /usr/local/share/awscli/examples

Bash completion has been installed to:
  /usr/local/etc/bash_completion.d
==> libpq
libpq is keg-only, which means it was not symlinked into /usr/local,
because conflicts with postgres formula.

If you need to have libpq first in your PATH, run:
  echo 'export PATH="/usr/local/opt/libpq/bin:$PATH"' >> /Users/hankim/.bash_profile

For compilers to find libpq you may need to set:
  export LDFLAGS="-L/usr/local/opt/libpq/lib"
  export CPPFLAGS="-I/usr/local/opt/libpq/include"

For pkg-config to find libpq you may need to set:
  export PKG_CONFIG_PATH="/usr/local/opt/libpq/lib/pkgconfig"
==> python@3.10
Python has been installed as
  /usr/local/bin/python3.10

Unversioned and major-versioned symlinks `python`, `python3`, `python-config`, `python3-config`, `pip`, `pip3`, etc. pointing to
`python3.10`, `python3.10-config`, `pip3.10` etc., respectively, have been installed into
  /usr/local/opt/python@3.10/libexec/bin

You can install Python packages with
  pip3.10 install <package>
They will install into the site-package directory
  /usr/local/lib/python3.10/site-packages

tkinter is no longer included with this formula, but it is available separately:
  brew install python-tk@3.10

If you do not need a specific version of Python, and always want Homebrew's `python3` in your PATH:
  brew install python3

See: https://docs.brew.sh/Homebrew-and-Python
==> python@3.9
Python has been installed as
  /usr/local/bin/python3.9

Unversioned and major-versioned symlinks `python`, `python3`, `python-config`, `python3-config`, `pip`, `pip3`, etc. pointing to
`python3.9`, `python3.9-config`, `pip3.9` etc., respectively, have been installed into
  /usr/local/opt/python@3.9/libexec/bin

You can install Python packages with
  pip3.9 install <package>
They will install into the site-package directory
  /usr/local/lib/python3.9/site-packages

tkinter is no longer included with this formula, but it is available separately:
  brew install python-tk@3.9

If you do not need a specific version of Python, and always want Homebrew's `python3` in your PATH:
  brew install python3

See: https://docs.brew.sh/Homebrew-and-Python
==> llvm
To use the bundled libc++ please add the following LDFLAGS:
  LDFLAGS="-L/usr/local/opt/llvm/lib/c++ -Wl,-rpath,/usr/local/opt/llvm/lib/c++"

llvm is keg-only, which means it was not symlinked into /usr/local,
because macOS already provides this software and installing another version in
parallel can cause all kinds of trouble.

If you need to have llvm first in your PATH, run:
  echo 'export PATH="/usr/local/opt/llvm/bin:$PATH"' >> /Users/hankim/.bash_profile

For compilers to find llvm you may need to set:
  export LDFLAGS="-L/usr/local/opt/llvm/lib"
  export CPPFLAGS="-I/usr/local/opt/llvm/include"
==> curl
curl is keg-only, which means it was not symlinked into /usr/local,
because macOS already provides this software and installing another version in
parallel can cause all kinds of trouble.

If you need to have curl first in your PATH, run:
  echo 'export PATH="/usr/local/opt/curl/bin:$PATH"' >> /Users/hankim/.bash_profile

For compilers to find curl you may need to set:
  export LDFLAGS="-L/usr/local/opt/curl/lib"
  export CPPFLAGS="-I/usr/local/opt/curl/include"

For pkg-config to find curl you may need to set:
  export PKG_CONFIG_PATH="/usr/local/opt/curl/lib/pkgconfig"
==> azure-cli
Bash completion has been installed to:
  /usr/local/etc/bash_completion.d
==> perl
By default non-brewed cpan modules are installed to the Cellar. If you wish
for your modules to persist across updates we recommend using `local::lib`.

You can set that up like this:
  PERL_MM_OPT="INSTALL_BASE=$HOME/perl5" cpan local::lib
And add the following to your shell profile e.g. ~/.profile or ~/.zshrc
  eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib=$HOME/perl5)"

```
