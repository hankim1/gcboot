# Install & Set Up Tools

```bash
pip3 install aws-mfa # tools for AWS MFA
brew install direnv  # tools to import .env files in local dir
brew install terraform eksctl awscli # tools to manage resources
```
