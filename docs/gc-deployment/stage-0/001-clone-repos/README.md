# Clone Deployment Repo

> NOTE: Since documents regarding Azure and GCP are not updated for long while, we only cover AWS in here.

- AWS: https://gitlab.com/volterra/ves.io/infra-gc-aws.git
- Azure: https://gitlab.com/volterra/ves.io/terraform-templates/templates/fabric-cluster-gc-az
- GCP: https://gitlab.com/volterra/ves.io/terraform-templates/templates/fabric-cluster-gc-gcp
